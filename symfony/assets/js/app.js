/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
const $ = require('jquery');
require('../../node_modules/bootstrap/dist/css/bootstrap.css');
// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

$(document).ready(function () {
    // add images to navbar
    var elem1 = document.createElement("img");
    elem1.setAttribute("src", "/images/gifs/18.gif");
    elem1.setAttribute("height", "15px");
    elem1.setAttribute("width", "20px");
    elem1.setAttribute("alt", "gif!");

    var elem2 = document.createElement("img");
    elem2.setAttribute("src", "/images/gifs/new.gif");
    elem2.setAttribute("height", "15px");
    elem2.setAttribute("width", "20px");
    elem2.setAttribute("alt", "gif!");

    var elem3 = document.createElement("img");
    elem3.setAttribute("src", "/images/gifs/americanflag.gif");
    elem3.setAttribute("height", "15px");
    elem3.setAttribute("width", "20px");
    elem3.setAttribute("alt", "gif!");

    var elem4 = document.createElement("img");
    elem4.setAttribute("src", "/images/gifs/drudgesiren.gif");
    elem4.setAttribute("height", "23px");
    elem4.setAttribute("width", "23px");
    elem4.setAttribute("alt", "gif!");

    var elem5 = document.createElement("img");
    elem5.setAttribute("src", "/images/gifs/eye.gif");
    elem5.setAttribute("height", "15px");
    elem5.setAttribute("width", "25px");
    elem5.setAttribute("alt", "gif!");

    var elem6 = document.createElement("img");
    elem6.setAttribute("src", "/images/gifs/hot-fire-impact.gif");
    elem6.setAttribute("height", "15px");
    elem6.setAttribute("width", "40px");
    elem6.setAttribute("alt", "gif!");

    var elem7 = document.createElement("img");
    elem7.setAttribute("src", "/images/gifs/shopping-cart.gif");
    elem7.setAttribute("height", "25px");
    elem7.setAttribute("width", "20px");
    elem7.setAttribute("alt", "gif!");

    element = document.querySelectorAll('.header-top-bar ul li');
    for(i=0; i<element.length; i++){ element[i].classList.add("header-top-button"); }

    buttons = document.querySelectorAll('.header-top-button')

    buttons[0].insertAdjacentHTML('afterbegin', elem1.outerHTML); 
    buttons[1].insertAdjacentHTML('afterbegin', elem2.outerHTML); 
    buttons[2].insertAdjacentHTML('afterbegin', elem3.outerHTML);
    buttons[3].insertAdjacentHTML('afterbegin', elem4.outerHTML);
    buttons[4].insertAdjacentHTML('afterbegin', elem5.outerHTML);
    buttons[5].insertAdjacentHTML('afterbegin', elem6.outerHTML);
    buttons[6].insertAdjacentHTML('afterbegin', elem7.outerHTML); 

    setTimeout(() => {
        if(!sessionStorage.getItem('popup')){
            $('#exampleModalCenter').modal();
            sessionStorage.setItem('popup', true);
        }
    }, 3500);

    $('[data-toggle="popover"]').popover();

    var $uploader = $(".admin input[type='file']");
    var $preview = $(".admin .preview");

    $uploader.attr("hidden", true);
    $uploader.on("change", function () {
        $preview.empty();

        var curFile = $uploader[0].files;

        if (curFile.length !== 0) {
            $preview.append('<div class="thumbnail"></div><small class="form-text text-muted">Nom du fichier ' + curFile[0].name + '</small>');
            var thumbnail = document.createElement('img');
            thumbnail.src = window.URL.createObjectURL(curFile[0]);
            $(".preview .thumbnail").append(thumbnail);
        }
    })
});
