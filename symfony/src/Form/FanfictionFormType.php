<?php

namespace App\Form;

use App\Entity\Fanfiction;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FanfictionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('content', CKEditorType::class)
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Publiée' => 'published',
                    'En cours de relecture' => 'review',
                    'Refusée' => 'rejected',
                    'Envoyée' => 'submitted',
                )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fanfiction::class,
        ]);
    }
}
