<?php
namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilder 
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->checker = $authorizationChecker;
    }

    public function mainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Accueil', ['route' => 'index', 'class' => 'test']);
        $menu->addChild('Actualités', ['route' => 'news']);
        $menu->addChild('Histoire', ['route' => 'history']);
        $menu->addChild('Ennemis', ['route' => 'ennemies']);
        $menu->addChild('Galerie', ['route' => 'gallery']);
        $menu->addChild('Fanfictions', ['route' => 'fanfictions']);
        $menu->addChild('Shop', [ 'linkAttributes' => ['target' => '_blank'], 'uri' => 'https://shop.donaldjtrump.com/collections/apparel']);

        return $menu;
    }
    
    public function userMenu(RequestStack $requestStack){

        $menu = $this->factory->createItem('root');

        if ($this->checker->isGranted('IS_AUTHENTICATED_ANONYMOUSLY') && !$this->checker->isGranted('ROLE_USER')) {
            $menu->addChild('Connexion', array('route' => 'fos_user_security_login'));
            $menu->addChild('Inscription', array('route' => 'fos_user_registration_register'));
        }else{
            
            if ($this->checker->isGranted('ROLE_ADMIN')) {
                $menu->addChild('Administration', array('route' => 'admin'));
            }
                $menu->addChild('Mon profil', array('route' => 'fos_user_profile_show'));
                $menu->addChild('Modifier mon profil', array('route' => 'fos_user_profile_edit'));
                $menu->addChild('Déconnexion', array('route' => 'fos_user_security_logout'));
            }

        
        return $menu;
    }
}