<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\GalleryImage;
use App\Form\GalleryImageFormType;
use App\Repository\GalleryImageRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GalleryImageController extends AbstractController
{
    public function adminGalleryImage(GalleryImageRepository $rep)
    {
        $galleryImgs = $rep->findAll();

        return $this->render('admin/gallery/show_gallery_images.html.twig', [
            'galleryImgs' => $galleryImgs,
        ]);
    }

    public function newGalleryImage(Request $request)
    {
        $galleryImg = new GalleryImage();
        $form = $this->createForm(GalleryImageFormType::class, $galleryImg, array('image' => true));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($galleryImg);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_gallery_images'));
        }

        return $this->render('admin/gallery/new_gallery_image.html.twig', [
            'form' => $form->createView(),
            'action' => 'Créer'
        ]);
    }

    public function editGalleryImg(Request $request, GalleryImage $galleryImg){

        $form = $this->createForm(GalleryImageFormType::class, $galleryImg, array('image' => false));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_gallery_images'));
        }

        return $this->render('admin/gallery/new_gallery_image.html.twig', [
            'form' => $form->createView(),
            'action' => 'Editer'
        ]);
    }

    public function deleteGalleryImg(GalleryImage $galleryImg)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($galleryImg);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_show_gallery_images'));
    }

    public function indexGalleryImage(Request $request, GalleryImageRepository $eRep, PaginatorInterface $paginator)
    {
        $allGalleryImgQuery = $eRep->createQueryBuilder('g')
            ->orderBy('g.createdAt', 'DESC')
            ->getQuery();;
    
        $pagination = $paginator->paginate(
            $allGalleryImgQuery, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
    
        // parameters to template
        return $this->render('gallery/index.html.twig', array('pagination' => $pagination));
    }

    /**
     * Display gallery images by given category name
     *
     * @param Request $request
     * @ParamConverter("cat", options={"mapping": {"cat_name": "name"}})
     * @param GalleryImageRepository $eRep
     * @param PaginatorInterface $paginator
     * @return void
     */
    public function indexGalleryImageCat(Request $request, Category $cat, GalleryImageRepository $eRep, PaginatorInterface $paginator)
    {
        $allGalleryImgQuery = $eRep->createQueryBuilder('g')
            ->andWhere('g.category = :val')
            ->setParameter('val', $cat)
            ->orderBy('g.createdAt', 'DESC')
            ->getQuery();;
    
        $pagination = $paginator->paginate(
            $allGalleryImgQuery, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
    
        // parameters to template
        return $this->render('gallery/index.html.twig', array('pagination' => $pagination, 'cat' => $cat->getName()));
    }
}
