<?php

namespace App\Controller;

<<<<<<< HEAD
=======
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\Sonata\MediaBundle\Entity\Gallery;
>>>>>>> develop
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
<<<<<<< HEAD
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('base.html.twig');
=======
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $articles = $this->getDoctrine()
        ->getRepository(Article::class)
        ->findAll(array('limit'=>5));

        return $this->render('home/index.html.twig', array(
            'articles'  => $articles
        ));
    }
    
    /**
     * @Route("/actualites", name="news")
     */
    public function blog(){
        $news = $this->getDoctrine()
                    ->getRepository(Article::class)
                    ->findByCategory('News');

        return $this->render('blog/index.html.twig', array(
            'articles'  =>  $news
        ));
    }

    /**
     * @Route("/fanfictions", name="fanfictions")
     */
    public function fanfictions(){
        $fanfictions = $this->getDoctrine()
                    ->getRepository(Article::class)
                    ->findByCategory('Fanfiction');

        return $this->render('blog/index.html.twig', array(
            'articles'  =>  $fanfictions
        ));
    }

    /**
     * @Route("/histoire", name="history")
     */
    public function historyAction(){
        return $this->render('history/index.html.twig');
>>>>>>> develop
    }
}
