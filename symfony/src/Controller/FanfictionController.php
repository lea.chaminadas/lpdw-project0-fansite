<?php

namespace App\Controller;

use App\Entity\Fanfiction;
use Cocur\Slugify\Slugify;
use App\Form\FanfictionFormType;
use App\Form\FanfictionSubmitFormType;
use App\Repository\FanfictionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FanfictionController extends AbstractController
{
    public function adminFanfiction(FanfictionRepository $ffRep){
        $fanfictions = $ffRep->findAll();

        return $this->render('admin/fanfictions/show_fanfictions.html.twig', [
            'fanfictions'  =>  $fanfictions
        ]);
    }

    public function newFanfiction(Request $request){
        $fanfiction = new Fanfiction();
        $form = $this->createForm(FanfictionFormType::class, $fanfiction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fanfiction->setAuthor($this->getUser());

            $slugify = new Slugify();
            $fanfiction->setSlug($slugify->slugify($fanfiction->getTitle()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fanfiction);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_fanfictions'));
        }

        return $this->render('admin/fanfictions/new_fanfiction.html.twig', array(
            'form' => $form->createView(),
            'action' => 'Créer'
        ));
    }


    public function editFanfiction(Request $request, Fanfiction $fanfiction){

        $form = $this->createForm(FanfictionFormType::class, $fanfiction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $fanfiction->setUpdatedAt(new \Datetime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_fanfictions'));
        }

        return $this->render('admin/fanfictions/new_fanfiction.html.twig', array(
            'form' => $form->createView(),
            'action' => 'Editer'
        ));
    }

    public function deleteFanfiction(Fanfiction $fanfiction)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($fanfiction);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_show_fanfictions'));
    }

    public function indexFanfiction(FanfictionRepository $ffRep, Request $request, PaginatorInterface $paginator){
        $allFanfictionsQuery = $ffRep->createQueryBuilder('f')
            ->where('f.status = :status')
            ->setParameter('status', 'published')
            ->orderBy('f.createdAt', 'DESC')
            ->getQuery();
    
        $pagination = $paginator->paginate(
            $allFanfictionsQuery, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
    
        // parameters to template
        return $this->render('fanfictions/index.html.twig', array('pagination' => $pagination));
    }

    public function singleFanfiction(Request $request, Fanfiction $fanfiction){

        return $this->render('fanfictions/single_fanfiction.html.twig', array(
            'fanfiction' => $fanfiction
        ));
    }

    public function submitFanfiction(Request $request){
        $fanfiction = new Fanfiction();
        $form = $this->createForm(FanfictionSubmitFormType::class, $fanfiction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fanfiction->setAuthor($this->getUser());

            $slugify = new Slugify();
            $fanfiction->setSlug($slugify->slugify($fanfiction->getTitle()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fanfiction);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('submit_fanfiction'));
        }

        return $this->render('fanfictions/submit_fanfiction.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
