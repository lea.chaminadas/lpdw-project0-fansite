<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    public function adminUser()
    {
        $users = $this->getDoctrine()->getManager()->getRepository(User::class)->findAll();

        return $this->render('admin/users/show_users.html.twig', [
            'users' => $users,
        ]);
    }

    public function newUser(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_user'));
        }


        return $this->render('admin/users/new_user.html.twig', [
            'form' => $form->createView(),
            'action' => 'Créer'
        ]);
    }

     public function editUser(Request $request, User $user){

        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_user'));
        }
        
        return $this->render('admin/users/new_user.html.twig', [
            'form' => $form->createView(),
            'action' => 'Editer'
        ]);
    }

    public function deleteUser(User $user)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_show_user'));
    }
}
