<?php
namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleFormType;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ArticleController extends AbstractController
{
    public function blog(ArticleRepository $artRep, Request $request, PaginatorInterface $paginator){
        $allArticlesQuery = $artRep->createQueryBuilder('a')
            ->orderBy('a.created', 'DESC')
            ->getQuery();
    
        $pagination = $paginator->paginate(
            $allArticlesQuery, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('blog/index.html.twig', array(
            'pagination'  =>  $pagination
        ));
    }
    
    public function showArticlesList(ArticleRepository $accRep){
        $articles = $accRep->findAll();

        return $this->render('admin/articles/show_articles.html.twig', [
            'articles'  =>  $articles
        ]);
    }

    public function newArticle(Request $request){
        $article = new Article();
        $form = $this->createForm(ArticleFormType::class, $article, array('thumbnail'=>true));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_articles'));
        }

        return $this->render('admin/articles/new_article.html.twig', array(
            'form' => $form->createView(),
            'action' => 'Créer'
        ));
    }

    public function editArticle(Request $request, Article $article){

        $form = $this->createForm(ArticleFormType::class, $article, array('thumbnail'=>false));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setUpdated(new \Datetime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirect($this->generateUrl('admin_show_articles'));
        }

        return $this->render('admin/articles/new_article.html.twig', array(
            'form' => $form->createView(),
            'action' => 'Editer'
        ));
    }

    public function deleteArticle(Article $article)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($article);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_show_articles'));
    }
}